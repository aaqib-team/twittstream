﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using TwitterStream;
using Unity_Common_Lib;

namespace Unity.SocketCommunicator
{
    public struct SocketResult
    {
        public bool success;
        public int errorCode;
        public string errorMessage;
        public Exception ex;
        public Message response;

        public override bool Equals(object obj)
        {
            if (obj.GetType().IsAssignableFrom(typeof(SocketResult)))
            {
                SocketResult other = (SocketResult)obj;
                return (
                    this.success == other.success &&
                    this.errorCode == other.errorCode &&
                    this.errorMessage == other.errorMessage &&
                    this.ex == other.ex &&
                    this.response == other.response
                    );
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            return success.GetHashCode() ^ errorCode.GetHashCode() ^ errorMessage.GetHashCode() ^ ex.GetHashCode() ^ response.GetHashCode();
        }

        public static bool operator ==(SocketResult point1, SocketResult point2)
        {
            return point1.Equals(point2);
        }

        public static bool operator !=(SocketResult point1, SocketResult point2)
        {
            return !point1.Equals(point2);
        }
    }

    public delegate void ConnectionCallback(SocketResult result);

    public sealed class SocketManager : IDisposable
    {
        public bool IsUnitActive = false;

        //Errors
        private static readonly string ERROR_SOCKET_EXCEPTION_MSG = "Look up error code at http://msdn.microsoft.com/en-us/library/windows/desktop/ms740668(v=vs.85).aspx";
        private static readonly int ERROR_CLIENT_CLOSED = -1;
        private static readonly string ERROR_CLIENT_CLOSED_MSG = "Tcp Client has been closed and cannot be reopened.";
        private static readonly int ERROR_NOT_CONNECTED = -2;
        private static readonly string ERROR_NOT_CONNECTED_MSG = "You are not connected";
        private static readonly int ERROR_CANT_WRITE = -3;
        private static readonly string ERROR_CANT_WRITE_MSG = "Stream not writable";
        private static readonly int ERROR_CANT_READ = -4;
        private static readonly string ERROR_CANT_READ_MSG = "Stream not readable";
        private static readonly int ERROR_NO_HELLO = -5;
        private static readonly string ERROR_NO_HELLO_MSG = "Central did not say hello properly";

        public static readonly String SERVER_NAME = "CENTRAL";
#if DEBUG
        private readonly bool LOG_MESSAGES = true;
#else
        private readonly bool LOG_MESSAGES = false;
#endif

        private Thread messageListenerThread;
        private Thread connectThread;
        private TcpClient client;
        private String serverHost;
        private int serverPort;
        private bool connected = false;
        private bool expectingPing = true;
        private bool readIncoming = true;

        private Message _message; //Current Message.

        private System.Threading.Timer pingTimer;

        public void SetExpectingPing()
        {
            expectingPing = true;
        }
        public bool IsConnected()
        {
            return connected;
        }

        public SocketManager()
        {
            Reset();

            serverHost = Config.configData.CentralIP;
            serverPort = Config.configData.CentralPort;

            LogEntry.WriteToLog("Central IP: " + serverHost + ", Server Port: " + serverPort, LogEntry.LogType.WARNING);

            messageListenerThread = new Thread(new ThreadStart(thread_ReadIncoming));
            messageListenerThread.Name = "Incoming Message Listener";
            messageListenerThread.Start();

            pingTimer = new System.Threading.Timer(pingTimer_callback, null, 30 * 1000, Timeout.Infinite);

            AsyncConnect();
        }

        public void Reset()
        {
            client = new TcpClient();
            client.SendTimeout = 10000;
            client.ReceiveTimeout = 10000;
        }

        private void pingTimer_callback(object param)
        {
            try
            {
                //Pause Timer
                pingTimer.Change(Timeout.Infinite, Timeout.Infinite);

                LogEntry.WriteToLog("PING Central ...", LogEntry.LogType.WARNING);
                PingCentral();
            }
            catch (Exception) { }

            pingTimer.Change(30 * 1000, Timeout.Infinite);
        }

        private void PingCentral()
        {
            try
            {
                SocketResult socketResult = SyncSendMessage(SocketManager.CreateMessage(SocketManager.SERVER_NAME, Unity_Common_Lib.Message.PING, true));

                if (!socketResult.success)
                {
                    LogEntry.WriteToLog("Ping failed to send: " + socketResult.errorMessage, LogEntry.LogType.ERROR);

                    if(!connected) //Disconnected.
                    {
                        LogEntry.WriteToLog("Not Connected to Central!", LogEntry.LogType.ERROR);
                        //Reconnect to CENTRAL.
                        //readIncoming = false;
                        Reset();
                        AsyncConnect();
                    }
                }
                else
                {
                    SetExpectingPing();
                    LogEntry.WriteToLog("Ping sent!", LogEntry.LogType.SUCCESS);
                }
            }
            catch (Exception) { }
        }

        public void AsyncConnect()
        {
            //string name;
            //string result;
            // bool active = UnitRegistration.IsUnitActiveMAC(Program.Controller.Unit.MAC, out name, out result);
            //Program.Controller.Log("SocketManager. Unit is Active so allow connection to Unity Central.", LOG_TYPE.INFO);
            // if (active)
            //{
            connectThread = new Thread(new ParameterizedThreadStart(thread_AsyncConnect));
            connectThread.Name = "Socket Connection Thread";
            connectThread.Start();
            //}
        }
        private void thread_AsyncConnect(object param)
        {
            SocketResult result = new SocketResult();

            try
            {

                LogEntry.WriteToLog("Connecting To Central ...", LogEntry.LogType.WARNING);
                //Try and connect
                connected = false;
                client.SendTimeout = 10000;
                client.ReceiveTimeout = 10000;
                client.Connect(serverHost, serverPort);

                LogEntry.WriteToLog("Sending CONNECT Message.", LogEntry.LogType.WARNING);
                //If we get a hello send CONNECT
                Message connectMessage = CreateMessage(SERVER_NAME, Message.CONNECT, false);

                //Add unit type and name to payload
                List<byte> payload = new List<byte>();
                payload.Add(Message.UNIT_SHARP);
                payload.AddRange(Encoding.UTF8.GetBytes(Config.configData.TwitterStreamName).ToList());

                connectMessage.AddBytePayload(payload.ToArray());
                SocketResult sentMessage = SyncSendMessage(connectMessage);

                if (sentMessage.success == true)
                {
                    connected = true;
                    result.success = true;
                }
            }
            catch (SocketException e)
            {
                result.success = false;
                result.errorCode = e.ErrorCode;
                result.errorMessage = ERROR_SOCKET_EXCEPTION_MSG;
                result.ex = e;
            }
            catch (ObjectDisposedException e)
            {
                result.success = false;
                result.errorCode = ERROR_CLIENT_CLOSED;
                result.errorMessage = ERROR_CLIENT_CLOSED_MSG;
                result.ex = e;

            }
            catch (Exception e)
            {
                result.success = false;
                result.errorCode = ERROR_NO_HELLO;
                result.errorMessage = ERROR_NO_HELLO_MSG;
                result.ex = e;

            }

            connectThread = null;
        }

        public void Dispose()
        {
            Dispose(false);
        }
        public void Dispose(bool temporary)
        {
            try
            {
                readIncoming = false;

                if (connectThread != null && connectThread.IsAlive)
                {
                    connectThread.Join(30000);
                    if (connectThread != null && connectThread.IsAlive) Process.GetCurrentProcess().Kill();
                }
                if (messageListenerThread != null && messageListenerThread.IsAlive)
                {
                    messageListenerThread.Join(30000);
                    if (messageListenerThread != null && messageListenerThread.IsAlive) Process.GetCurrentProcess().Kill();
                }

                SyncClose(temporary);
                readIncoming = false;
            }
            catch (Exception) { }
        }
        public void SyncClose(bool temporary)
        {
            try
            {
                if (client.Connected)
                {
                    //If temporary it means we are restarting for some reason
                    Message message = CreateMessage(SERVER_NAME, Message.DISCONNECT, false);
                    if (temporary) message.AddStringPayload("RESTARTING");

                    if (connected) SyncSendMessage(message);
                    client.Close();
                    connected = false;
                }
            }
            catch (Exception) { }
        }

        public static Message CreateMessage(string to, string type, bool expectingResponse)
        {
            try
            {
                return new Message(Config.configData.TwitterStreamName, to, type, expectingResponse);
            }
            catch (Exception) { }

            return null;
        }

        public SocketResult SyncSendMessage(Message message)
        {
            SocketResult result = new SocketResult();
            NetworkStream stream = null;

            try
            {
                //Get the network stream
                stream = client.GetStream();

                //Convert the message to bytes
                byte[] bytes = message.ToBytes();

                if (stream.CanWrite)
                {
                    //Program.Controller.Log("Writing Stream to Central.", LOG_TYPE.INFO);
                    //Write the message to the stream
                    stream.Write(bytes, 0, bytes.Length);
                    stream.Flush();
                    result.success = true;
                }
                else
                {
                    result.success = false;
                    result.errorCode = ERROR_CANT_WRITE;
                    result.errorMessage = ERROR_CANT_WRITE_MSG;
                    return result;
                }

                return result;
            }
            catch (ObjectDisposedException e)
            {
                result.success = false;
                result.errorCode = ERROR_CLIENT_CLOSED;
                result.errorMessage = ERROR_CLIENT_CLOSED_MSG;
                result.ex = e;
                connected = false;
                return result;
            }
            catch (InvalidOperationException e)
            {
                result.success = false;
                result.errorCode = ERROR_NOT_CONNECTED;
                result.errorMessage = ERROR_NOT_CONNECTED_MSG;
                result.ex = e;
                connected = false;
                return result;
            }
            catch (System.IO.IOException e)
            {
                result.success = false;
                result.ex = e;

                if (e.InnerException.GetType() == typeof(SocketException))
                {
                    SocketException ex = (SocketException)e.InnerException;
                    result.errorCode = ex.ErrorCode;
                    result.errorMessage = ERROR_SOCKET_EXCEPTION_MSG;
                    result.ex = ex;
                }
                connected = false;

                return result;
            }
        }

        public SocketResult SyncReadMessage()
        {
            SocketResult result = new SocketResult();
            NetworkStream stream = null;
            List<Byte> bytes = new List<Byte>();

            try
            {
                //Get the network stream
                stream = client.GetStream();

                if (stream.CanRead)
                {
                    //Read message from stream
                    Stopwatch stopwatch = Stopwatch.StartNew();
                    int b = -2;
                    bool completeMessage = false;

                    //While we haven't timed out and haven't received an ETX
                    while (stopwatch.ElapsedMilliseconds < client.ReceiveTimeout)
                    {
                        if (stream.DataAvailable)
                        {
                            b = stream.ReadByte();

                            if (b < 0)
                            {
                                //Invalid message (i.e. reached end of stream without ETX)
                                break;
                            }
                            else if (b == 3)
                            {
                                //Received ETX so break out
                                completeMessage = true;
                                break;
                            }
                            else
                            {
                                //Reset timeout as we have received some data
                                stopwatch.Restart();
                                bytes.Add((Byte)b);
                            }
                        }
                        else
                        {
                            //Sleep for 200ms before trying again
                            Thread.Sleep(200);
                        }
                    }

                    //Convert to message
                    try
                    {
                        if (bytes.Count > 0)
                        {
                            if (!completeMessage) throw new Exception();

                            byte[] buff = bytes.ToArray();

                            Message msg = Message.FromBytes(buff);

                            if (msg == null)// || (msg.To() != Program.Controller.Unit.PCName && msg.To() != "*"))
                            {
                                throw new Exception();
                            }
                            else
                            {
                                if (LOG_MESSAGES) LogEntry.WriteToLog("Received message from " + msg.From(), LogEntry.LogType.WARNING);

                                result.success = true;
                                result.response = msg;
                                return result;
                            }
                        }
                        else
                        {
                            string error = "No message received";
                            result.success = false;
                            result.errorMessage = error;
                            return result;
                        }
                    }
                    catch (Exception e)
                    {
                        string error = "Invalid message received from " + SERVER_NAME;
                        if (LOG_MESSAGES) LogEntry.WriteToLog(error, LogEntry.LogType.ERROR);

                        result.success = false;
                        result.errorMessage = error;
                        return result;
                    }
                }
                else
                {
                    result.success = false;
                    result.errorCode = ERROR_CANT_READ;
                    result.errorMessage = ERROR_CANT_READ_MSG;
                    return result;
                }
            }
            catch (ObjectDisposedException e)
            {
                result.success = false;
                result.errorCode = ERROR_CLIENT_CLOSED;
                result.errorMessage = ERROR_CLIENT_CLOSED_MSG;
                result.ex = e;
                connected = false;
                return result;
            }
            catch (InvalidOperationException e)
            {
                result.success = false;
                result.errorCode = ERROR_NOT_CONNECTED;
                result.errorMessage = ERROR_NOT_CONNECTED_MSG;
                result.ex = e;
                connected = false;
                return result;
            }
            catch (System.IO.IOException e)
            {
                result.success = false;
                result.ex = e;

                if (e.InnerException.GetType() == typeof(SocketException))
                {
                    SocketException ex = (SocketException)e.InnerException;
                    result.errorCode = ex.ErrorCode;
                    result.errorMessage = ERROR_SOCKET_EXCEPTION_MSG;
                    result.ex = ex;
                }
                connected = false;

                return result;
            }
        }

        private void thread_ReadIncoming()
        {
            Stopwatch pingWaiter = new Stopwatch();

            readIncoming = true;
            while (readIncoming)
            {
                if (connected)
                {
                    SocketResult result = SyncReadMessage();

                    if (expectingPing)
                    {
                        if (pingWaiter.IsRunning)
                        {
                            if (pingWaiter.ElapsedMilliseconds > client.ReceiveTimeout)
                            {
                                //if the timeout has expired assume we've lost connection
                                SocketResult r = new SocketResult();
                                r.success = false;
                                expectingPing = false;
                                pingWaiter.Stop();
                            }
                        }
                        else
                        {
                            pingWaiter.Restart();
                        }

                        //If we are waiting for a ping and we get one then stop waiting
                        if (result.success && result.response != null && result.response.Type() == Message.PING)
                        {
                            pingWaiter.Stop();
                            expectingPing = false;
                        }
                    }

                    //If we have a message
                    if (result.success && result.response != null)
                    {
                        LogEntry.WriteToLog("Message received from Central: " + result.response.Type() + ((result.response.PayloadAsString().Length > 0) ? " - " + result.response.PayloadAsString() : ""), LogEntry.LogType.SUCCESS);
                        _message = result.response;
                        //Start a handler thread and continue listening
                        SocketMessageHandler.AsyncHandle(result.response);
                    }
                }
                else
                {
                    expectingPing = false;
                    pingWaiter.Stop();
                }

                Thread.Sleep(200);
            }

            messageListenerThread = null;
        }

        /// <summary>
        /// Get's latest Message from CENTRAL.
        /// </summary>
        /// <returns></returns>
        public Message GetMessage()
        {
            return _message;
        }



    }
}