﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using TwitterStream;
using Unity_Common_Lib;

namespace Unity.SocketCommunicator
{
    class SocketMessageHandler
    {
        private SocketMessageHandler() { }

        public static void AsyncHandle(Message message)
        {
            Thread t = new Thread(new ParameterizedThreadStart(thread_AsyncHandle));
            t.Name = "Message Handler thread";
            object[] paras = { message };
            t.Start(paras);
        }

        private static void thread_AsyncHandle(object paras)
        {
            object[] para = (object[])paras;
            Message msg = (Message)para[0];

            switch (msg.Type())
            {
                case Message.UNIT_COMMAND:
                    //Program.Controller.ExecuteCommand(msg.PayloadAsString(), msg.From());

                    LogEntry.WriteToLog("Socket MSG Received: " + msg.PayloadAsString(), LogEntry.LogType.INFO);

                    if(msg.PayloadAsString().IndexOf("vote", StringComparison.CurrentCultureIgnoreCase) >=0)
                    {
                        string[] voteArray = msg.PayloadAsString().Split(new string[] { "vote" }, StringSplitOptions.None);

                        int voteIndex = Convert.ToInt32(voteArray[1]);
                        LogEntry.WriteToLog("Voted Index: " + voteIndex, LogEntry.LogType.INFO);

                        MainWindow.twitterHandle.InteractiveFloorVoted(voteIndex);
                    }
                    break;
            }
        }
    }
}
