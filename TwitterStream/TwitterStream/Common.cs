﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterStream
{
    class Common
    {


        public static void WriteTextToFile(string text, string path, bool append)
        {
            try
            {

                string dir = Path.GetDirectoryName(path);

                if (!Directory.Exists(dir))
                {
                    Directory.CreateDirectory(dir);
                }

                if (!File.Exists(path))
                {
                    File.Create(path).Dispose();
                }

                using (StreamWriter file = new StreamWriter(path, append))
                {
                    file.Write(text);
                }

            }
            catch (Exception e)
            {
                LogEntry.WriteToLog("Unable to Write Text To File. ERR: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        /// <summary>
        /// Read all text from file.
        /// Returns empty String if file doesn't exist or blank file.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static String ReadAllTextFromFile(string path)
        {
            try
            {
                return File.ReadAllText(path);
            }
            catch(Exception e)
            {

            }
            return String.Empty;
        }

    }
}
