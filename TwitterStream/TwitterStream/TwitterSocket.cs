﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Unity_Common_Lib;

namespace TwitterStream
{
    class TwitterSocket
    {

        /// <summary>
        /// Sends Test Tweet
        /// </summary>
        public static void SendTest()
        {

            try
            {
                //Message m = new Message(Program.Controller.Unit.macFilter.mac, to, type, expectingResponse);
                Message m = new Message(Config.configData.TwitterStreamName, Config.configData.FlashClient, Message.UNIT_COMMAND, false);
                m.AddStringPayload("Hello Test Tweet!");
                MainWindow.socketManager.SyncSendMessage(m);
                LogEntry.WriteToLog("Sent Test Message!", LogEntry.LogType.SUCCESS);
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("ERR Sending Test Command: "+e.Message, LogEntry.LogType.ERROR);
            }
        }

        /// <summary>
        /// Send Tweet To Flash Client.
        /// </summary>
        /// <param name="tweet"></param>
        public static void SendTweet(string tweet)
        {

            try
            {
                //Message m = new Message(Program.Controller.Unit.macFilter.mac, to, type, expectingResponse);
                Message m = new Message(Config.configData.TwitterStreamName, Config.configData.FlashClient, Message.UNIT_COMMAND, false);
                m.AddStringPayload(tweet);
                MainWindow.socketManager.SyncSendMessage(m);
                //LogEntry.WriteToLog("Sent Message: "+ tweet, LogEntry.LogType.SUCCESS);
            }
            catch (Exception e)
            {
                LogEntry.WriteToLog("ERR Sending Test Command: " + e.Message, LogEntry.LogType.ERROR);
            }
        }


    }
}
