﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;

namespace TwitterStream
{
    /**
     * Handles all log entries to the console.
     */
    public class LogEntry
    {
        static LogEntry _logEntry = null;

        public static string LOG_DIRECTORY_PATH = Config.RootDirectory + @"logs\";

        private System.Threading.Timer logClearTimer;
        private int logClear_Interval = 3600; //Every hour clear the log.

        public enum LogType
        {
            INFO = 0,
            WARNING,
            ERROR,
            SUCCESS
        };

        /**
         * Constructor.
         * @param logfield - 'Textbox'.
         */
        public LogEntry()
        {
            if (_logEntry == null)
            {
                _logEntry = this;

                logClearTimer = new System.Threading.Timer(LogClear_Interval, null, logClear_Interval * 1000, Timeout.Infinite);

                //Paragraph p = _logField.Document.Blocks.FirstBlock as Paragraph;
                //p.LineHeight = 1;
            }
        }

        private void LogClear_Interval(object param)
        {
            try
            {
                logClearTimer.Change(Timeout.Infinite, Timeout.Infinite); //Pause Timer
                ClearLog();
            }
            catch (Exception) { }

            logClearTimer.Change(logClear_Interval * 1000, Timeout.Infinite);
        }

        private void ClearLog()
        {
            try
            {
                MainWindow.mw.ClearLog();

            }
            catch (Exception) { }
        }

        /**
         * Write To Log.
         * @param log = Information you want to output onto the console.
         * @param lType = Log Type - INFO, WARNING, ERROR or SUCCESS.
         */
        public static void WriteToLog(String log, LogType lType)
        {
            try
            {
                MainWindow.mw.Log(log, lType);
                WriteToFile(log);
            }
            catch(Exception e)
            {

            }
        }

        private static void WriteToFile(string text)
        {
            try
            {
                string dateNow = DateTime.Now.ToString("dd-MM-yyyy");

                if(!Directory.Exists(LOG_DIRECTORY_PATH))
                {
                    Directory.CreateDirectory(LOG_DIRECTORY_PATH);
                }

                string filepath = LOG_DIRECTORY_PATH + dateNow + ".txt";

                string logText = "";
                if (File.Exists(filepath))
                {
                    logText += Environment.NewLine;
                }

                logText += DateTime.Now + " - " + text;

                Common.WriteTextToFile(logText, filepath, true);
            }
            catch (Exception e)
            {

            }

        }

        /**
         * Returns string as first letter uppercase.
         */
        public static String UppercaseFirst(string s)
        {
            // Check for empty string.
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            // Return char and concat substring.
            return char.ToUpper(s[0]) + s.Substring(1);
        }

        /**
         * Get Instance of LogEntry. - Only once instance will be defined.
         * @return class instance.
         */
        private static LogEntry GetInstance()
        {
            return _logEntry;
        }

    }
}