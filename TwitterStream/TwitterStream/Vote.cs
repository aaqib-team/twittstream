﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterStream.Data;

namespace TwitterStream
{
    public class Vote
    {
        public List<VoteData> voteData = new List<VoteData>();

        public Vote()
        {
            try
            {
                //Initialize Vote Dictionary.
                InitVote();
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("HashTag() ERR: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        /// <summary>
        /// Initialize Hashtag Vote Counts.
        /// </summary>
        public void InitVote()
        {
            try
            {
                voteData = new List<VoteData>();

                List<VoteData> votes = LoadVotes();

                if(votes != null)
                {
                    voteData = votes;
                }
                else
                {
                    LogEntry.WriteToLog("Unable to Load Vote Data. Initialize Vote Count as 0.", LogEntry.LogType.WARNING);

                    for (int i = 0; i < Config.configData.HashTags.Count; i++)
                    {
                        string hashtag = Config.configData.HashTags[i];

                        //Initialize with default vote count as 0.
                        VoteData vd = new VoteData();
                        vd.hashtag = hashtag;
                        vd.count = 0;

                        voteData.Add(vd);
                    }

                    SaveVoteToFile();
                }
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("ERR Init Vote: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        /// <summary>
        /// Increments vote count on hashtag by 1.
        /// Saves updated vote to file.
        /// </summary>
        /// <param name="hashtag"></param>
        public void AddVote(string hashtag)
        {
            try
            {
                VoteData vd = GetVote(hashtag);
                vd.count = vd.count + 1;
                LogEntry.WriteToLog("Hashtag: " + hashtag + ", Vote Count: "+vd.count, LogEntry.LogType.INFO);
                SaveVoteToFile();
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("ERR Adding Vote: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        public VoteData GetVote(string hashtag)
        {
            try
            {
                for(int i=0;i<voteData.Count;i++)
                {
                    VoteData vd = voteData[i];
                    if(vd.hashtag.Equals(hashtag, StringComparison.OrdinalIgnoreCase))
                    {
                        return vd;
                    }
                }
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("ERR Getting Vote: " + e.Message, LogEntry.LogType.ERROR);
            }
            return null;
        }


        /// <summary>
        /// Load Votes from file.
        /// </summary>
        /// <returns></returns>
        public List<VoteData> LoadVotes()
        {
            try
            {
                string votes = Common.ReadAllTextFromFile(Config.VoteFile);
                LogEntry.WriteToLog("Votes Loaded: " + votes, LogEntry.LogType.INFO);

                if (String.IsNullOrEmpty(votes) || String.IsNullOrWhiteSpace(votes))
                {
                    return null;
                }
                else
                {
                    List<VoteData> voteData = JsonConvert.DeserializeObject<List<VoteData>>(votes);
                    if (voteData != null) //Check if Config Data has been loaded correctly.
                    {
                        LogEntry.WriteToLog("Vote Data Loaded Correctly!", LogEntry.LogType.INFO);
                        return voteData;
                    }
                    else //Config Data hasn't been loaded correctly.
                    {
                        return null;
                    }
                }
            }
            catch (Exception e)
            {
                LogEntry.WriteToLog("Unable to Load Vote Data: " + e.Message, LogEntry.LogType.ERROR);
            }
            return null;
        }

        public void SaveVoteToFile()
        {
            try
            {
                string voteJSON = JsonConvert.SerializeObject(voteData, Formatting.Indented);
                Common.WriteTextToFile(voteJSON, Config.VoteFile, false);
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("Unable to Save Vote Data To File: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

    }
}
