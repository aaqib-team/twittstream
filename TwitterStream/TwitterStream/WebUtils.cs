﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;

namespace TwitterStream
{
    class WebUtils
    {

        public static bool CheckForInternetConnection()
        {
            try
            {
                using (var client = new WebClient())
                {
                    using (var stream = client.OpenRead("http://www.google.com"))
                    {
                        LogEntry.WriteToLog("Yes, Connected to the Internet!", LogEntry.LogType.SUCCESS);
                        return true;
                    }
                }
                LogEntry.WriteToLog("Not Connected to the Internet!", LogEntry.LogType.ERROR);
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("Not Connected to the Internet: "+e.Message, LogEntry.LogType.ERROR);
                return false;
            }
        }

    }
}
