﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwitterStream.Data;

namespace TwitterStream
{
    class Config
    {
        public static string RootDirectory = @"C:\TwitterStream\";
        public static string ConfigFile = RootDirectory + @"config.json";
        public static string VoteFile = RootDirectory + @"votes.json";

        public static ConfigData configData = null;

        public Config()
        {

            try
            {
                ConfigData confData = LoadConfigFile();
                if (confData == null)
                {
                    LogEntry.WriteToLog("Config is null!", LogEntry.LogType.INFO);
                    SetDefaultConfig();
                }
                else
                {
                    LogEntry.WriteToLog("Config File loaded correctly!!!", LogEntry.LogType.INFO);

                    configData = confData;
                }
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("Config ERR: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        /// <summary>
        /// Load Config from File if exist.
        /// </summary>
        /// <returns></returns>
        public ConfigData LoadConfigFile()
        {
            try
            {
                string config = Common.ReadAllTextFromFile(ConfigFile);
                LogEntry.WriteToLog("Config: " + config, LogEntry.LogType.INFO);

                if(String.IsNullOrEmpty(config) || String.IsNullOrWhiteSpace(config))
                {
                    return null;
                }
                else
                {
                    ConfigData configData = JsonConvert.DeserializeObject<ConfigData>(config);
                    if (configData != null) //Check if Config Data has been loaded correctly.
                    {
                        LogEntry.WriteToLog("Config Loaded Correctly!", LogEntry.LogType.INFO);
                        return configData;
                    }
                    else //Config Data hasn't been loaded correctly.
                    {
                        return null;
                    }
                }
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("Unable to Load Config Data: " + e.Message, LogEntry.LogType.ERROR);
            }
            return null;
        }

        /// <summary>
        /// Set Default Config Data.
        /// </summary>
        public void SetDefaultConfig()
        {
            try
            {
                configData = new ConfigData();
                configData.TwitterStreamName = "TwitterStPC0";
                configData.FlashClient = "FLASH0000001";
                configData.CentralIP = "localhost";
                configData.CentralPort = 7000;

                List<string> hashtags = new List<string>();
                hashtags.Add("#test1");
                hashtags.Add("#test2");
                configData.HashTags = hashtags;

                configData.ConsumerKey = "";
                configData.ConsumerSecret = "";
                configData.UserAccessToken = "";
                configData.UserAccessSecret = "";

                SaveConfigData(); //Save Config Data to file.
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("Unable to Set Default Config Data: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        /// <summary>
        /// Save Config Data to File.
        /// </summary>
        private void SaveConfigData()
        {
            try
            {
                if (configData != null)
                {
                    string configJSON = JsonConvert.SerializeObject(configData, Formatting.Indented);
                    Common.WriteTextToFile(configJSON, ConfigFile, false);
                    LogEntry.WriteToLog("Config Values saved!", LogEntry.LogType.SUCCESS);
                }
            }
            catch (Exception e)
            {
                LogEntry.WriteToLog("Unable to Save Config Data: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

    }
}
