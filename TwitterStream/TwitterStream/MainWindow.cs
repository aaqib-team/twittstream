﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Unity.SocketCommunicator;
using System.Threading;

namespace TwitterStream
{
    public partial class MainWindow : Form
    {

        public static MainWindow mw = null;
        public static SocketManager socketManager = null;
        public static TwitterHandle twitterHandle = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            try
            {
                mw = this;
                
                LogEntry.WriteToLog("Twitter Application Started ", LogEntry.LogType.INFO);

                Thread t1 = new Thread(() =>
                {
                    bool connected = WebUtils.CheckForInternetConnection();
                    if (connected)
                    {
                        LogEntry.WriteToLog("Connected to the Internet!", LogEntry.LogType.SUCCESS);
                    }
                    else
                    {
                        LogEntry.WriteToLog("Error connecting to the internet!", LogEntry.LogType.ERROR);
                    }

                });
                t1.Start();


                Config config = new Config();
                LogEntry logEntry = new LogEntry();

                twitterHandle = new TwitterHandle();
                socketManager = new SocketManager();
            }
            catch (Exception e2)
            {

            }
        }

        public void Log(string message, LogEntry.LogType type)
        {
            if (applicationLog.IsDisposed) return;
            if (message == null) return;

            try
            {
                applicationLog.Invoke((MethodInvoker)delegate
                {
                    // default log colour
                    Color logColor = Color.Black;

                    // determine the log type
                    switch (type)
                    {
                        case LogEntry.LogType.INFO:
                            logColor = Color.Black;
                            break;
                        case LogEntry.LogType.ERROR:
                            logColor = Color.Red;
                            break;
                        case LogEntry.LogType.SUCCESS:
                            logColor = Color.Green;
                            break;
                        case LogEntry.LogType.WARNING:
                            logColor = Color.Orange;
                            break;
                        default:
                            break;
                    }

                    // switch the colour to highlight the current timestamp
                    applicationLog.Select(applicationLog.TextLength, 0);
                    applicationLog.SelectionColor = Color.Blue;
                    string timestamp = DateTime.Now.ToString(@"MM\/dd\/yyyy @ HH:mm:ss") + ": ";

                    // add the timestamp
                    applicationLog.AppendText(timestamp);

                    // swtich the colour back to the log type colour
                    applicationLog.SelectionColor = logColor;

                    // update the log textbox
                    applicationLog.AppendText(message + Environment.NewLine);
                    applicationLog.ScrollToCaret();
                });
            }
            catch (ObjectDisposedException)
            {

            }
            catch (InvalidOperationException)
            {

            }
            catch (Exception)
            {

            }
        }

        public void ClearLog()
        {
            try
            {
                if (mw.InvokeRequired)
                {
                    mw.Invoke((MethodInvoker)delegate
                    {
                        ClearLogInvoke();
                    });
                }
                else
                {
                    ClearLogInvoke();
                }
            }
            catch (Exception) { }
        }

        private void ClearLogInvoke()
        {
            try
            {
                applicationLog.Clear();
            }
            catch (Exception) { }
        }

    }
}
