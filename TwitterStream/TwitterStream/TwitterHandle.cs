﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Tweetinvi;
using Tweetinvi.Models;
using Tweetinvi.Streaming;
using TwitterStream.Data;

namespace TwitterStream
{
    public class TwitterHandle
    {

        private List<Tweett> tweets = new List<Tweett>();
        private List<TweetPlusTimestamp> tweetsPlusTimestamp = new List<TweetPlusTimestamp>();

        private static string TweetsDir = Config.RootDirectory + @"tweets/";
        //private static string TweetsLocalFile = Config.RootDirectory +  @"tweets.json";
        public Vote votes = null;

        private System.Threading.Timer twitterTimer;

        private static IFilteredStream stream;

        private int streamStateInterval = 5; //Every 5 seconds check stream state. 

        private string dateFormat = "ddMMyyyyHH00";

        public TwitterHandle()
        {
            try
            {

                Thread t1 = new Thread(() =>
                {
                    LoadTweetsFromLocalFile();
                    StartStream();

                });
                t1.Start();

                votes = new Vote();
                twitterTimer = new System.Threading.Timer(twitterTimer_callback, null, streamStateInterval * 1000, Timeout.Infinite);
            }
            catch (Exception e)
            {
                LogEntry.WriteToLog("ERR TwitterHandle: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        /// <summary>
        /// Publish 'Hello World' Test Nessage,
        /// </summary>
        public void PublishHelloWorld()
        {
            try
            {
                // Set up your credentials (https://apps.twitter.com)
                GetTwitterCredentials();

                // Publish the Tweet "Hello World" on your Timeline
                Tweet.PublishTweet("Hello World From NUC!");

                LogEntry.WriteToLog("Published Hello World!", LogEntry.LogType.INFO);
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("ERR Publishing Hello World: "+e.Message, LogEntry.LogType.ERROR);
            }
        }

        public ITwitterCredentials GetTwitterCredentials()
        {
            /*return Auth.SetUserCredentials("UFwcR5LLRA67xi7yx7MLMvRAC",
                                    "IHycY2qPaUiVPlLsZTZQuvXd2pigZzJkqKIpjeeXiFoLFvd1Nu",
                                    "2872469811-KGdYZl0zr1rwzX5yIq4lWtqz103kSSwBiKjdnUw",
                                    "3bLIFNarXc9RJLkYMb2WcN3p3mLgTsr953oF6YTdxX8jO");*/
            
            return Auth.SetUserCredentials(Config.configData.ConsumerKey,
                                    Config.configData.ConsumerSecret,
                                    Config.configData.UserAccessToken,
                                    Config.configData.UserAccessSecret);
        }

        /// <summary>
        /// Start Twitter Stream.
        /// </summary>
        public void StartStream()
        {

            try
            {
                ITwitterCredentials twitterCredentials = GetTwitterCredentials();
                if (twitterCredentials != null)
                {
                    LogEntry.WriteToLog("Starting Twitter Stream ...", LogEntry.LogType.INFO);
                    //Tweet.PublishTweet("Hello World From C# 2!");


                    stream = Stream.CreateFilteredStream(twitterCredentials);


                    for (int i = 0; i < Config.configData.HashTags.Count; i++)
                    {
                        string hashtag = Config.configData.HashTags[i];
                        stream.AddTrack(hashtag);
                        LogEntry.WriteToLog("Hashtag Added: "+hashtag + " To Stream.", LogEntry.LogType.SUCCESS);
                    }

                    stream.FilterLevel = Tweetinvi.Streaming.Parameters.StreamFilterLevel.None;
                    stream.MatchingTweetReceived += (sender, args) =>
                    {
                        ITweet tweet = args.Tweet;
                        //LogEntry.WriteToLog(tweet.ToJson(), LogEntry.LogType.WARNING);
                        Tweett t = new Tweett();
                        t.Id = tweet.Id;
                        t.username = tweet.CreatedBy.ScreenName;
                        t.name = tweet.CreatedBy.Name;
                        t.text = tweet.FullText;
                        tweets.Add(t);

                        TweetPlusTimestamp tpt = new TweetPlusTimestamp();
                        tpt.tweet = t;
                        tpt.timestamp = DateTime.Now.ToString(dateFormat);
                        tweetsPlusTimestamp.Add(tpt);

                        LogEntry.WriteToLog("Tweet ID: " + tweet.IdStr + ". Tweet Found: " + tweet.FullText + ". Username: " + tweet.CreatedBy.ScreenName + ". Name: " + tweet.CreatedBy.Name, LogEntry.LogType.INFO);

                        SaveTweetsToLocalFile();

                        //TwitterSocket.SendTest();

                        string hashtag = GetHashtagFromTweet(tweet.FullText); //Get Hashtag from Tweet sent by user.
                        votes.AddVote(hashtag);

                        //string tweetJSON = JsonConvert.SerializeObject(t, Formatting.Indented);
                        //TwitterSocket.SendTweet(tweetJSON);
                        TweetPlusVote tweetPlusVote = new TweetPlusVote();
                        tweetPlusVote.tweet = t;
                        tweetPlusVote.votes = votes.voteData;
                        string tweetJSON = JsonConvert.SerializeObject(tweetPlusVote, Formatting.Indented);
                        LogEntry.WriteToLog(tweetJSON, LogEntry.LogType.WARNING);

                        TwitterSocket.SendTweet(tweetJSON);

                    };
                    stream.StreamStarted += stream_StreamStarted;
                    stream.StreamStopped += stream_StreamStopped;
                    stream.StreamPaused += stream_StreamPaused;
                    stream.StreamResumed += stream_StreamResumed;
                    stream.StartStreamMatchingAnyConditionAsync();
                    
                    //LogEntry.WriteToLog("Twitter Stream Started!", LogEntry.LogType.INFO);
                }
                else
                {
                    LogEntry.WriteToLog("Unable to start strem since Twitter Credentials is NULL", LogEntry.LogType.ERROR);
                }

            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("ERR Starting Twitter Stream: " + e.Message, LogEntry.LogType.ERROR);
            }

        }

        void stream_StreamResumed(object sender, EventArgs e)
        {
            try
            {
                LogEntry.WriteToLog("Stream Resumed!", LogEntry.LogType.WARNING);
            }
            catch (Exception e2)
            {
                LogEntry.WriteToLog("Stream Resumed ERR: " + e2.Message, LogEntry.LogType.WARNING);
            }
        }

        void stream_StreamPaused(object sender, EventArgs e)
        {
            try
            {
                LogEntry.WriteToLog("Stream Paused!", LogEntry.LogType.WARNING);
            }
            catch (Exception e2)
            {
                LogEntry.WriteToLog("Stream Paused ERR: " + e2.Message, LogEntry.LogType.WARNING);
            }
        }

        void stream_StreamStopped(object sender, Tweetinvi.Events.StreamExceptionEventArgs e)
        {
            try
            {
                LogEntry.WriteToLog("Stream Stopped!", LogEntry.LogType.ERROR);
            }
            catch (Exception e2)
            {
                LogEntry.WriteToLog("Stream Stopped ERR: " + e2.Message, LogEntry.LogType.ERROR);
            }
        }

        void stream_StreamStarted(object sender, EventArgs e)
        {
            try
            {
                LogEntry.WriteToLog("Stream Started!", LogEntry.LogType.SUCCESS);
            }
            catch(Exception e2)
            {
                LogEntry.WriteToLog("Stream Started ERR: "+e2.Message, LogEntry.LogType.ERROR);
            }
        }

        public string GetHashtagFromTweet(string tweet)
        {
            try
            {
                for(int i=0;i<Config.configData.HashTags.Count;i++)
                {
                    string hashtag = Config.configData.HashTags[i];

                    if(tweet.IndexOf(hashtag, StringComparison.CurrentCultureIgnoreCase) >=0)
                    {
                        return hashtag;
                    }
                }
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("ERR retrieving Hashtag from tweet: " + e.Message, LogEntry.LogType.ERROR);
            }
            return String.Empty;
        }


        /// <summary>
        /// Save Jobs locally to file.
        /// </summary>
        public void SaveTweetsToLocalFile()
        {
            try
            {

                removeOldTweetsFromMemory();
                string tweetsJSON = JsonConvert.SerializeObject(tweets, Formatting.Indented);

                //LogEntry.WriteToLog("Tweets: " + tweetsJSON, LogEntry.LogType.INFO);
                //LogEntry.WriteToLog("Saving Tweets To Local File: " + TweetsLocalFile, LogEntry.LogType.INFO);

                string dtNow = DateTime.Now.ToString(dateFormat);

                string path = TweetsDir + @"tweet-" + dtNow + ".json";
                LogEntry.WriteToLog("Path: " + path, LogEntry.LogType.WARNING);

                Common.WriteTextToFile(tweetsJSON, path, false);
            }
            catch (Exception e)
            {
                LogEntry.WriteToLog("Unable to Save Tweets locally. ERR: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        /// <summary>
        /// Interactive Floor Voted.
        /// </summary>
        /// <param name="index"></param>
        public void InteractiveFloorVoted(int index)
        {
            try
            {
                string hashtag = Config.configData.HashTags[index]; //Get Hashtag of voted index.
                LogEntry.WriteToLog("Vote from Interactive Floor: "+hashtag, LogEntry.LogType.WARNING);

                votes.AddVote(hashtag); //Add as vote.
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("ERR processing Interactive floor vote: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        /// <summary>
        /// Load Tweets for current hour from local file.
        /// </summary>
        public void LoadTweetsFromLocalFile()
        {
            try
            {
                string dtNow = DateTime.Now.ToString("ddMMyyyyHH00");
                string path = TweetsDir + @"tweet-" + dtNow + ".json";
                string text = Common.ReadAllTextFromFile(path);

                List<Tweett> tweets = JsonConvert.DeserializeObject<List<Tweett>>(text);
                if(tweets != null && tweets.Count > 0)
                {
                    this.tweets = tweets;

                    tweetsPlusTimestamp = new List<TweetPlusTimestamp>();
                    for (int i = 0; i < tweets.Count; i++)
                    {
                        Tweett t = tweets[i];
                        TweetPlusTimestamp tpt = new TweetPlusTimestamp();
                        tpt.tweet = t;
                        tpt.timestamp = DateTime.Now.ToString(dateFormat);
                        tweetsPlusTimestamp.Add(tpt);
                    }

                    LogEntry.WriteToLog("Tweets Loaded From File:" + text, LogEntry.LogType.INFO);
                }
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("Unable to Load Tweets from local file: " + e.Message, LogEntry.LogType.ERROR);
            }
        }

        private void twitterTimer_callback(object param)
        {
            try
            {
                if(stream.StreamState == StreamState.Stop || stream.StreamState == StreamState.Pause) //Stop or Paused -> Start Stream again.
                {
                    LogEntry.WriteToLog("Stream has been stopped! -> Restart Stream", LogEntry.LogType.WARNING);
                    stream.StartStreamMatchingAnyConditionAsync();
                }
            }
            catch (Exception) { }

            twitterTimer.Change(streamStateInterval * 1000, Timeout.Infinite);
        }

        private void removeOldTweetsFromMemory()
        {
            try
            {
                for(int i=0;i<tweetsPlusTimestamp.Count;i++)
                {
                    TweetPlusTimestamp tpt = tweetsPlusTimestamp[i];
                    string dateNow = DateTime.Now.ToString(dateFormat);

                    LogEntry.WriteToLog("DateNow: " + dateNow, LogEntry.LogType.WARNING);
                    LogEntry.WriteToLog("Tweet Id: " + tpt.tweet.Id + ", date: "+tpt.timestamp, LogEntry.LogType.WARNING);

                    if(!dateNow.Equals(tpt.timestamp, StringComparison.OrdinalIgnoreCase))
                    {
                        LogEntry.WriteToLog("Removing Tweet (id) from memory: " + tpt.tweet.Id, LogEntry.LogType.WARNING);
                        //Remove Tweet from memory as it's not within the hour.
                        tweets.Remove(tpt.tweet);
                    }
                }
            }
            catch(Exception e)
            {
                LogEntry.WriteToLog("ERR Removing Old Tweets from Memory:" + e.Message, LogEntry.LogType.ERROR);
            }
        }

    }
}
