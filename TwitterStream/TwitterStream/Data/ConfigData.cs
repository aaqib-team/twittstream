﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterStream.Data
{
    class ConfigData
    {

        public string TwitterStreamName
        {
            get;
            set;
        }

        public string FlashClient
        {
            get;
            set;
        }

        public string CentralIP
        {
            get;
            set;
        }

        public int CentralPort
        {
            get;
            set;
        }

        public List<string> HashTags
        {
            get;
            set;
        }

        public string ConsumerKey
        {
            get;
            set;
        }

        public string ConsumerSecret
        {
            get;
            set;
        }

        public string UserAccessToken
        {
            get;
            set;
        }

        public string UserAccessSecret
        {
            get;
            set;
        }

    }
}
